<?php

require_once 'vendor/autoload.php';
require_once 'kanooh/WebDriverTestCase.php';

class WebDriverDemo extends kanooh\WebDriverTestCase
{

  protected $base_url = 'http://product.acc.kanooh.be';

  protected function doLogin($username, $password)
  {
    $this->url('/user');
    $this->byName('name')->value($username);
    $this->byName('pass')->value($password);
    $this->byCss('input.form-submit')->click();

    $loggedin = $this->byClassName('logged-in')->size();

    $this->assertTrue($loggedin > 0,'User is logged in');

  }


  public function testLogin()
  {
    $this->doLogin('demo','demo');
  }


}
