<?php
  /**
   * A abstract test class based from
   * http://stackoverflow.com/questions/13972304/dynamically-setting-the-browsers-static-property-in-sauce-io-phpunit-selenium-t
   */
namespace kanooh;

class WebDriverTestCase extends \Sauce\Sausage\WebDriverTestCase {
  public static $browsers = array();

  public static function suite($className) {
    self::browserSetup();
    return \PHPUnit_Extensions_SeleniumTestSuite::fromTestCaseClass($className);
  }

  public static function browserSetup() {

    switch(getenv('environment')) {

      case 'jenkins': break;

      // Default is actually localhost.
      default:
        self::$browsers = array(

          // run Chrome locally
          array(
            'browserName' => 'chrome',
            'local' => true,
            'sessionStrategy' => 'shared'
          ),

          // run Chrome locally
          array(
            'browserName' => 'firefox',
            'local' => true,
            'sessionStrategy' => 'shared'
          )
        );
        break;

    }

  }
}